function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + ' tackled '+ target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
	target.health -= this.attack
		if(target.health <= 5) {
			target.faint()
		}
	} 
	this.faint = function() {
		console.log(this.name + " fainted, ");
	}
}

let alakazam = new Pokemon("Alakazam", 20);
let altaria = new Pokemon("Altaria", 8);
let ampharos = new Pokemon("Ampharos", 1)


alakazam.tackle(altaria)
altaria.tackle(ampharos)
ampharos.tackle(alakazam)
alakazam.tackle(alakazam)
altaria.tackle(alakazam)
